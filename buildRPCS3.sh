#!/bin/bash
set -euo pipefail
# Based on https://github.com/RPCS3/rpcs3/blob/master/BUILDING.md
LANG=C
sudo pacman -Syu --noconfirm --needed glew openal cmake vulkan-validation-layers qt5-base qt5-declarative sdl2
cd ~
rm -rf ./rpcs3 ./rpcs3_build
git clone https://github.com/RPCS3/rpcs3.git
cd rpcs3
git submodule update --init
cd ../ && mkdir rpcs3_build && cd rpcs3_build
cmake ../rpcs3/ && make -j$(nproc)
sudo mv ~/rpcs3_build/bin/* /usr/local/sbin/
rm -rf ~/rpcs3 ~/rpcs3_build
