#!/bin/bash
set -euo pipefail

# Libloot deps
pacman -Syu --needed --noconfirm python python-pip git cmake base-devel boost cargo cbindgen wget http-parser
# Additional LOOT deps
pacman -S --needed --noconfirm libx11 pango libxrandr libxcomposite nss libxdamage libxcursor libxi libxtst libxss atk alsa-lib yarn
# Build libloot
cd /root
rm -rf libloot
git clone https://github.com/loot/libloot.git
cd libloot
mkdir build
cd build
cmake ..
make -j$(nproc) all
# Package libloot
tar -czvf /tmp/libloot-compiled.tar.gz libloot.so ../include
# Build LOOT
cd /root
rm -rf loot
git clone https://github.com/loot/loot
cd loot
yarn install
# Change prebuilt LOOT path to our own local archive
sed -i s/"https:\/\/github.com\/loot\/libloot\/releases\/download\/0.14.10\/libloot.tar.xz"/"file:\/\/\/tmp\/libloot-compiled.tar.gz"/ CMakeLists.txt
mkdir build
cd build
cmake ..
make -j$(nproc) all

# Test by copying /root/loot/build and running ./LOOT from it
