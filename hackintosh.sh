# Used on a macOS (Catalina - 10.15) VM with 60GB storage but even that was cutting it close - I recommend 70GB or more.
# https://github.com/kholia/OSX-KVM

# Sys Prefs>Security & Privacy>Privacy tab>Full Disk Access>Add Utilities>Terminal
sudo systemsetup -setremotelogin on

# Partially based on osxdaily.com, partially on random guides and posts on the net
# https://osxdaily.com/2020/12/14/how-create-macos-big-sur-iso/

# Cleanup to make sure no directory previously exists to break this process
sudo rm -rf /Volumes/Dummy /Volumes/EFI /Volumes/BigSur /tmp/OpenCore

# If the size bloated since I wrote this, you will notice when running createinstallmedia - it does a size check and refuses to run if so
sudo hdiutil create -o /tmp/BigSur -size 13300m -volname Dummy -layout SPUD -fs HFS+J

# Note outputted diskname or run `diskutil list` to see which disk you need to work with
sudo hdiutil attach /tmp/BigSur.dmg -noverify -mountpoint /Volumes/Dummy

# This is a workaround to get an EFI partition on the image
# The hdiutil create command above only creates a HFS partition without an EFI one which we need
# Format the Dummy volume we just created
diskutil eraseDisk JHFS+ BigSur /dev/disk4

# https://support.apple.com/en-us/HT201372
sudo /Applications/Install\ macOS\ Big\ Sur.app/Contents/Resources/createinstallmedia --volume /Volumes/BigSur --nointeraction

# Mount ESP
sudo diskutil mount /dev/disk4s1

# Grab an up to date version of Open Core from Github, 0.7.0 as of time of writing
#https://github.com/acidanthera/opencorepkg/releases
curl -L https://github.com/acidanthera/OpenCorePkg/releases/download/0.7.0/OpenCore-0.7.0-RELEASE.zip > /tmp/OpenCore.zip
unzip /tmp/OpenCore.zip -d /tmp/OpenCore
sudo cp -r /tmp/OpenCore/X64/EFI /Volumes/EFI/
# Copy sample config.plist
sudo cp /tmp/OpenCore/Docs/Sample.plist /Volumes/EFI/EFI/OC/config.plist

# config.plist will not work as is and requires manual changes for your specific hardware, find a page for it, my case:
# https://dortania.github.io/OpenCore-Install-Guide/config-laptop.plist/ivy-bridge.html#starting-point
# Among other things, you'll need to find the correct SmBIOS for your HW
# https://dortania.github.io/OpenCore-Install-Guide/extras/smbios-support.html#macbook
# Skip all of this for ISO creation though, you can do this after you flash this onto a flashdrive

# Attempt to unmount the disk, if this does not happen cleanly, copy the image from tmp to /Users, reboot and edit path following steps to reflect it
# sudo cp /tmp/BigSur.dmg /Users/${USER}/BigSur.dmg
sudo diskutil unmountDisk /dev/disk4
sudo hdiutil detach /dev/disk4
sudo hdiutil convert /tmp/BigSur.dmg -format UDTO -o /Users/${USER}/BigSur.cdr
sudo mv /Users/${USER}/BigSur.cdr /Users/${USER}/BigSur.iso
# Copy the final image from host
rsync -av --progress c0rn3j@192.168.1.220:~/BigSur.iso ~/

Notes:
Apple File System (APFS): The file system used by macOS 10.13 or later.
Mac OS Extended(JHFS+ - Journaled HFS Plus): The file system used by macOS 10.12 or earlier.
Use OpenCore over Clover - Clover uses OC starting with Big Sur anyways - https://dortania.github.io/OpenCore-Install-Guide/why-oc.html
