#!/usr/bin/env bash
set -euo pipefail
udpTest() { # $1 IP $2 port
	if mpv --no-video --volume=0 --network-timeout=1 --length=0.5 udp://${1}:${2} &>/dev/null; then
		echo "${1}:${2} works"
		return 0
	else
		echo "${1}:${2} broken"
		return 1
	fi
}
# Interesting third octets: 0 6 7 8 10 22 50
for i in $(seq 0 9); do
	for j in $(seq 0 99); do
		paddedPort=$(printf '%02d' "${j}")
		if udpTest "239.232.${i}.${j}" "8${i}${paddedPort}"; then
			echo baf > /dev/null
		fi
	done
done

for i in $(seq 10 99); do
	for j in $(seq 0 99); do
		paddedPort=$(printf '%02d' "${j}")
		if udpTest "239.232.${i}.${j}" "1${i}${paddedPort}"; then
			echo baf > /dev/null
		fi
	done
done
