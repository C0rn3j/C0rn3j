#!/bin/python
import sys

# Example ./GravityFalls_AlphabetDecode.py "1-2-3-4-5 5-8"
#ABCDE EH

# I was bored, okay?

message = str(sys.argv[1])
wordArray = message.split(" ")
result = ""
for word in wordArray:
	splitWord = word.split("-")
	for letter in splitWord:
		result += chr( int(letter) + 64 )
	result += " "
print(result)
