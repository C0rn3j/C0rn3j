#!/bin/bash
set -euo pipefail

# Get mouse position:
#xev -id $(xwininfo -name "GemCraft Frostborn Wrath"| awk '/Window id/ { print $4 }')

# Get key list:
#xmodmap -pke

# Assuming 1920x1080

StartGame () {
	if [[ -n ${1-} ]]; then
		case ${1} in
			1 )
				# Start game and continue from first slot
				xdotool mousemove --window ${game} 100 950 mousedown 1 click 1 sleep 1.5 \
					mousemove --window ${game} 500 270 click 1 sleep 2;;
			* )
				Log "Error in StartGame[1] - ${1} is not a valid argument"
				exit 1;;
		esac
	else
		Log "StartGame - undefined variable"
		exit 1
	fi
	Log "Continuing game in slot ${1}"
}
Select () {
	if [[ -n ${1-} ]]; then
		case ${1} in
			A1 )
				# Move to the right-bottom of the map screen
				xdotool mousemove --window ${game} 1910 1070 mousedown 1 sleep 0.1 mousemove --window ${game} 10 10 sleep 0.1 mouseup 1 \
								mousemove --window ${game} 1910 1070 mousedown 1 sleep 0.1 mousemove --window ${game} 10 10 sleep 0.1 mouseup 1 \
								mousemove --window ${game} 1910 1070 mousedown 1 sleep 0.1 mousemove --window ${game} 10 10 sleep 0.1 mouseup 1
				# Assuming we're at the right-bottom of the map screen, open Field A1
				xdotool mousemove --window ${game} 800 240 click 1 sleep 1;;
			* )
				Log "Error in Select[1] - ${1} is not a valid argument"
				exit 1;;
		esac
	else
		Log "Select - undefined variable"
		exit 1
	fi
	Log "Selected Field ${1}"
}
SelectMode () {
	if [[ -n ${1-} ]]; then
		case ${1} in
			journey )   xdotool mousemove --window ${game} 500 200 click 1;;
			endurance ) xdotool mousemove --window ${game} 800 200 click 1;;
			trial )     xdotool mousemove --window ${game} 1100 200 click 1;;
			* )
				Log "Error in Select[1] - ${1} is not a valid argument"
				exit 1;;
		esac
	else
		Log "SelectMode - undefined variable"
		exit 1
	fi
	Log "Selected ${1} Mode"
}
StartBattle () {
	xdotool mousemove --window ${game} 1440 880 click 1 sleep 7
	Log "Starting the battle"
}
CreateGem () {
	case ${1} in
		1 ) xdotool key KP_End;;
		2 ) xdotool key KP_Down;;
		3 ) xdotool key KP_Next;;
		4 ) xdotool key KP_Left;;
		5 ) xdotool key KP_Begin;;
		6 ) xdotool key KP_Right;;
		* )
			Log "Error in CreateGem[1] - ${1} is not a valid argument"
			exit 1;;
	esac
	Log "Created gem ${1}"
}
Build () {
	case ${1} in
		wall ) buildingKey="w";;
		tower ) buildingKey="t";;
		amplifier ) buildingKey="a";;
		trap ) buildingKey="r";;
		lantern ) buildingKey="l";;
		pylon ) buildingKey="p";;
		* )
			Log "Error in Build[1] - ${1} is not a valid argument"
			exit 1;;
	esac
	xdotool key ${buildingKey}
	xdotool mousemove --window ${game} ${2} ${3} click 1 sleep 0.05 click 3 sleep 0.05
	if [[ ${1} == "amplifier" ]]; then
		Log "Built an ${1}"
	else
		Log "Built a ${1}"
	fi
	if [[ -n ${4-} ]]; then
		if [[ ${4} == "fill" ]]; then
			xdotool mousemove --window ${game} ${2} ${3} click 1 sleep 0.05
			Log "Filled the ${1} with a gem"
		fi
	fi
	if [[ -n ${5-} ]]; then
		case ${5} in
			orb )        x=0;   y=-10;;
			giants )     x=10;  y=0;;
			swarmlings ) x=10;  y=-10;;
			random )     x=10;  y=10;;
			structure )  x=0;   y=10;;
			special )    x=-10; y=10;;
			shielded )   x=-10; y=0;;
			lowhp )      x=-10; y=-10;;
			* )
				Log "Error in Build[5] - ${5} is not a valid argument"
				exit 1;;
		esac
		xdotool mousedown 3 sleep 0.1 mousemove_relative -- ${x} ${y} sleep 0.1 mouseup 3
		Log "Set the ${1} to prioritize ${5}"
	fi
}
CreatedGemLevel () {
	xdotool mousemove --window ${game} 1820 410 click --repeat 20 5 click --repeat $((${1}-1)) 4
	Log "Set default created gem level to ${1}"
}
SetTraits() {
	if [[ ${traitsPreviouslySet} == "yes" ]]; then
		Log "Traits are already set, continuing"
		return
	fi
	traitsPreviouslySet="yes"
	# Reset traits
	xdotool mousemove --window ${game} 520 880 click 1
	if [[ ${1} -gt 0 ]]; then xdotool mousemove --window ${game} 220   360 click --repeat ${1} 4; fi
	if [[ ${2} -gt 0 ]]; then xdotool mousemove --window ${game} 520   360 click --repeat ${2} 4; fi
	if [[ ${3} -gt 0 ]]; then xdotool mousemove --window ${game} 820   360 click --repeat ${3} 4; fi
	if [[ ${4} -gt 0 ]]; then xdotool mousemove --window ${game} 1120  360 click --repeat ${4} 4; fi
	if [[ ${5} -gt 0 ]]; then xdotool mousemove --window ${game} 1420  360 click --repeat ${5} 4; fi
	if [[ ${6} -gt 0 ]]; then xdotool mousemove --window ${game} 220   540 click --repeat ${6} 4; fi
	if [[ ${7} -gt 0 ]]; then xdotool mousemove --window ${game} 520   540 click --repeat ${7} 4; fi
	if [[ ${8} -gt 0 ]]; then xdotool mousemove --window ${game} 820   540 click --repeat ${8} 4; fi
	if [[ ${9} -gt 0 ]]; then xdotool mousemove --window ${game} 1120  540 click --repeat ${9} 4; fi
	if [[ ${10} -gt 0 ]]; then xdotool mousemove --window ${game} 1420 540 click --repeat ${10} 4; fi
	if [[ ${11} -gt 0 ]]; then xdotool mousemove --window ${game} 220  720 click --repeat ${11} 4; fi
	if [[ ${12} -gt 0 ]]; then xdotool mousemove --window ${game} 520  720 click --repeat ${12} 4; fi
	if [[ ${13} -gt 0 ]]; then xdotool mousemove --window ${game} 820  720 click --repeat ${13} 4; fi
	if [[ ${14} -gt 0 ]]; then xdotool mousemove --window ${game} 1120 720 click --repeat ${14} 4; fi
	if [[ ${15} -gt 0 ]]; then xdotool mousemove --window ${game} 1420 720 click --repeat ${15} 4; fi
	Log "Set traits to ${*}"
}
StartRound() {
	xdotool key --repeat 2 q
	Log "Starting the round"
}
Upgrade() {
	xdotool mousemove --window ${game} ${1} ${2} key u
}
Buff() {
	if [[ -n ${3-} ]]; then
		case ${3} in
			bolt )    buffKey=4;;
			beam )    buffKey=5;;
			barrage ) buffKey=6;;
			* )
				Log "Error in Buff[3] - ${3} is not a valid argument"
				exit 1;;
		esac
	else
		Log "Buff - undefined variable"
		exit 1
	fi
	xdotool mousemove --window ${game} ${1} ${2} key ${buffKey}
}
NextWave () {
	xdotool key n sleep 0.25
	Log "Sending next wave"
}
Log () {
	echo "$(date)  ${1}" # | tee -a ${logFile}
}
BuildPhase () {
	if [[ -n ${1-} ]]; then
		case ${1} in
			A1 )
				amplifiers=( "380,510" "490,510" "380,620" "490,620" )
				towers=( "440,560" "440,510" "380,560" "500,560" "440,620" "1030,360" "240,150" )
				CreatedGemLevel 7
				for i in {1..9}; do CreateGem 4; done
				Build amplifier 380 510 fill
				Build tower     440 510 fill special
				Build amplifier 490 510 fill
				Build tower     380 560 fill lowhp
				Build tower     440 560 fill special
				Build tower     500 560 fill special
				Build amplifier 380 620 fill
				Build tower     440 620 fill special
				Build amplifier 490 620 fill
				# Backup tower for shadows and their swarmlings
				for i in {1..2}; do CreateGem 4; done
				Build tower     1030 360 fill special
				# Backup top tower for wizard hunters
				Build tower     240 150 fill special;;
			* )
				Log "Error in Build[1] - ${1} is not a valid argument"
				exit 1;;
		esac
	else
		Log "Build - undefined variable"
		exit 1
	fi
	Log "Finished building for phase ${1}"
}
GemBombMode() {
	xdotool key b sleep 0.25
	Log "Now in gem bomb mode"
}
EggRunLoop() {
	# This function is here for the Eggs Royale achievement
	Log "Entering Egg Run loop"
	while :; do
		Select A1
		SelectMode endurance
		StartBattle
		CreateGem 4
		GemBombMode
		Log "Starting egg search & destroy mission"
		eggHuntOutput=$( java -jar /usr/share/java/sikulixide/sikulixide-2.0.4.jar -r ./egg_hunt.py 2>/dev/null )
		Log "Egg hunt over"
		sleep 3
	done
}
game=$(xdotool search --name "^GemCraft Frostborn Wrath$")
xdotool windowactivate ${game}

StartGame 1
#EggRunLoop

traitsPreviouslySet="no"
while :; do
	GameStart=$(date +%s);
	Select A1
	SelectMode journey
	SetTraits \
		12 12 12 12 12 \
		10 0  12 0  12 \
		0  0  12 12 12
#	SelectMode endurance
#	SetTraits \
#		0  12 0 12 0 \
#		0  0  0 0  12 \
#		0  0  0 0  12
	StartBattle
	BuildPhase A1
	StartRound
	while :; do
		for t in "${towers[@]}"; do Upgrade ${t%,*} ${t##*,}; done
		for t in "${towers[@]}"; do Upgrade ${t%,*} ${t##*,}; Buff ${t%,*} ${t##*,} beam; done
		for a in "${amplifiers[@]}"; do Upgrade ${a%,*} ${a##*,}; done
		for i in {1..7}; do NextWave; done
		endButtonPos=$( java -jar /usr/share/java/sikulixide/sikulixide-2.0.4.jar -r ./sikuli.py 2>/dev/null )
		if [[ ${endButtonPos} == "None" ]]; then
			Log "BackToTheMap button not found, continuing..."
		else
			GameEnd=$(date +%s);
			Log "BackToTheMap button found, starting a new game!"
			Log "Time spent on this Game: $(echo $((GameEnd-GameStart)) | awk '{print int($1/60)":"int($1%60)}')"
			endButtonPos=$( echo "${endButtonPos}" | cut -c3- | cut -d ' ' -f 1)
			xdotool mousemove ${endButtonPos%,*} ${endButtonPos##*,} click 1
			sleep 5
			break
		fi
	done
done
