#include <ESP8266WiFi.h>
const short int LED1 = 10;
const char* ssid = "ssid";
const char* password = "password";
WiFiServer server(80);
void setup() {
	Serial.begin(115200);
	pinMode(LED1, OUTPUT);
	// WiFi.mode(m): set mode to WIFI_AP, WIFI_STA, or WIFI_AP_STA.
	WiFi.mode(WIFI_STA);
	delay(10);
	WiFi.begin(ssid, password);
	while (WiFi.status() != WL_CONNECTED) {
		Serial.println("Wi-Fi not connected, retrying... ");
		delay(500); // Do not use this delay in SoftAP mode
	}
	// Start the wifi server
	server.begin();
	Serial.println("IP address: ");
	Serial.println(WiFi.localIP());
}

void loop() {
	if(WiFi.status() != WL_CONNECTED) {
		Serial.println("Wi-Fi not connected, retrying... ");
		delay(500); // Do not use this delay in SoftAP mode
	}
	WiFiClient client = server.available();
	if (!client) {
		//Serial.println("No client connected, suiciding.");
		return;
	}

	int insanity = 0;
	// Wait until the client sends some data
	while (!client.available()) {
		insanity++;
		if (insanity == 1000) {
			Serial.println("And nobody came...");
			return;
		}
		delay(1);
		Serial.println("Waiting for client to send data.");
		//client = server.available(); // Check if the connection didn't fuck up, if yes, kill it.
		//if (!client) {
			//return;
		//}
	}
	Serial.println("Client available, receiving data...");

	// Read the first line of the request
	String request = client.readStringUntil('\r');
	client.flush();

	// Match the request
	if (request.indexOf("/OFF") != -1) {
		digitalWrite(LED1, LOW);
	}
	if (request.indexOf("/ON") != -1) {
		digitalWrite(LED1, HIGH);
	}
	// Return the response
	String html = String("HTTP/1.1 200 OK\r\n") +
                "Content-Type: text/html\r\n" +
                "\r\n" +
                "<!DOCTYPE HTML>" +
                "<html>" +
                "<head>" +
                "<style media=\"screen\" type=\"text/css\">" +
                "   .button {" +
                "        background-color: #000000;" +
                "        color: #FFFFFF;" +
                "        padding: 10px;" +
                "        border-radius: 10px;" +
                "        -moz-border-radius: 10px;" +
                "        -webkit-border-radius: 10px;" +
                "        margin:10px" +
                "    }"
                "    .small-btn {" +
                "        width: 50px;" +
                "        height: 25px;" +
                "    }" +
                "    .medium-btn {" +
                "        width: 70px;" +
                "        height: 30px;" +
                "    }" +
                "    .big-btn {" +
                "        width: 90px;" +
                "        height: 40px;" +
                "    }" +
                "</style>" +
                "</head>" +
                "<body>" +
                "<a href=\"/ON\"><div class=\"button big-btn\">ON</div></a>" +
                "<a href=\"/OFF\"><div class=\"button big-btn\">OFF</div></a>" +
                "</body>" +
                "</html>";
	client.print(html);
	delay(1);
}
