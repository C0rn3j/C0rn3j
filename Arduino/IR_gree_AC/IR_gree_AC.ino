#include <ESP8266WiFi.h>

#include <IRremoteESP8266.h>
#include <IRrecv.h>
#include <IRsend.h>
#include <IRtimer.h>
#include <IRutils.h>
#include <ir_Argo.h>
#include <ir_Daikin.h>
#include <ir_Fujitsu.h>
#include <ir_Gree.h>
#include <ir_Kelvinator.h>
#include <ir_LG.h>
#include <ir_Magiquest.h>
#include <ir_Midea.h>
#include <ir_Mitsubishi.h>
#include <ir_Toshiba.h>
#include <ir_Trotec.h>

const char* ssid = "NetdomaUp";
const char* password = "balalaika123456789aBcbalalaika";
WiFiServer server(80);

int RECV_PIN = 0; //IR IN
int SEND_PIN = 13;
IRrecv irrecv(RECV_PIN);
decode_results results;

IRsend irsend(SEND_PIN);

IRGreeAC ac(SEND_PIN);

void printState() {
	// Display the settings.
	Serial.println("Gree A/C remote is in the following state:");
	Serial.printf("  %s\n", ac.toString().c_str());
	// Display the encoded IR sequence.
	unsigned char* ir_code = ac.getRaw();
	Serial.print("IR Code: 0x");
	for (uint8_t i = 0; i < kMitsubishiACStateLength; i++)
		Serial.printf("%02X", ir_code[i]);
	Serial.println();
}

void setup() {
	Serial.begin(115200);
	ac.begin();
	irrecv.enableIRIn();
	irsend.begin();

	// WiFi.mode(m): set mode to WIFI_AP, WIFI_STA, or WIFI_AP_STA.
	WiFi.mode(WIFI_STA);
	delay(10);
	WiFi.begin(ssid, password);

	while (WiFi.status() != WL_CONNECTED) {
		Serial.println("Wi-Fi not connected, retrying... ");
		delay(500); // Do not use this delay in SoftAP mode
	}
	// Start the wifi server
	server.begin();
	Serial.println("IP address: ");
	Serial.println(WiFi.localIP());

	Serial.print("RECV_PIN now: ");
	Serial.println(RECV_PIN);

	Serial.println("Default state of the remote.");
	printState();

	Serial.println("Setting desired state for A/C.");
	ac.on();
	ac.setFan(1);
	ac.setMode(kGreeCool);
	ac.setTemp(24);
}
void loop() {
	// Send a random code (LG webOS INstart) to be picked up by the receiver
//	irsend.sendNEC(0x20DFDF20, 32);

	if (irrecv.decode(&results)) {
		Serial.println((long int)results.value, HEX);

/*     switch((long int)results.value) {
      case 0x20DFF10E : RollUp(32000); break;
      case 0x20DF0DF2 : RollUp(1000); break;
      case 0x20DF5DA2 : RollDown(1000); break;
      case 0x20DF718E : RollDown(28000); break;
     default : break;
      }
      */
		irrecv.resume(); // Receive the next value
		return; // Kill the cycle
	}

	if(WiFi.status() != WL_CONNECTED) {
		Serial.println("Wi-Fi not connected, retrying... ");
		delay(500); // Do not use this delay in SoftAP mode
	}
	Serial.println("Sending IR command to A/C ...");
	ac.send();
	printState();

	WiFiClient client = server.available();
	if (!client) {
//		Serial.println("No client connected, suiciding.");
		return;
	}

	int insanity = 0;
	// Wait until the client sends some data
	while (!client.available()) {
		insanity++;
		if (insanity == 1000) {
			Serial.println("And nobody came...");
			return;
		}
		delay(1);
		Serial.println("Waiting for client to send data.");
	}
	Serial.println("Client available, receiving data...");

	// Read the first line of the request
	String request = client.readStringUntil('\r');
	client.flush();

	// Match the request
	if (request.indexOf("/NEXT_PIN") != -1) {
		nextPin();
	}
	// Return the response
	String html = String("HTTP/1.1 200 OK\r\n") +
		"Content-Type: text/html\r\n" +
		"\r\n" +
		"<!DOCTYPE HTML>" +
		"<html>" +
		"<head>" +
		"<style media=\"screen\" type=\"text/css\">" +
		"   .button {" +
		"        background-color: #000000;" +
		"        color: #FFFFFF;" +
		"        padding: 10px;" +
		"        border-radius: 10px;" +
		"        -moz-border-radius: 10px;" +
		"        -webkit-border-radius: 10px;" +
		"        margin:10px" +
		"    }"
		"    .big-btn {" +
		"        width: 90px;" +
		"        height: 40px;" +
		"    }" +
		"</style>" +
		"</head>" +
		"<body>" +
		"<a href=\"/NEXT_PIN\"><div class=\"button big-btn\">NEXT_PIN</div></a>" +
		"</body>" +
		"</html>";
	client.print(html);
	delay(1);
}

void nextPin() {
	irrecv.disableIRIn();
	delay(100);
	RECV_PIN = RECV_PIN+1; //IR IN
	Serial.print("RECV_PIN now: ");
	Serial.println(RECV_PIN);
	IRrecv irrecv(RECV_PIN);
	decode_results results;
	irrecv.enableIRIn(); // Start the receiver
	delay(100);
}
