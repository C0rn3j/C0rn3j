#include <ESP8266WiFi.h>

#include <IRrecv.h>
#include <IRremoteESP8266.h>
#include <IRsend.h>
#include <IRtimer.h>
#include <IRutils.h>
#include <ir_Argo.h>
#include <ir_Daikin.h>
#include <ir_Fujitsu.h>
#include <ir_Kelvinator.h>
#include <ir_LG.h>
#include <ir_Magiquest.h>
#include <ir_Midea.h>
#include <ir_Mitsubishi.h>
#include <ir_Toshiba.h>
#include <ir_Trotec.h>

const char* ssid = "Netdoma";
const char* password = "balalaika123456789aBcbalalaika";

int RollDownPin = 5;
int secondLedPin = 2;
int RollUpPin = 4;
int LEDstate1 = 0;
int LEDstate2 = 1;
int LEDstate3 = 0;

void RollUp(int microtime) {
	digitalWrite(RollUpPin, 1);
	delay(microtime);
	digitalWrite(RollUpPin, 0);
}
void RollDown(int microtime) {
	digitalWrite(RollDownPin, 1);
	delay(microtime);
	digitalWrite(RollDownPin, 0);
}
int RECV_PIN = D6; //IR IN
IRrecv irrecv(RECV_PIN);
decode_results results;

WiFiServer server(80);

void setup() {
	irrecv.enableIRIn(); // Start the receiver

	// screen /dev/ttyUSB0 115200
	// ^a \ to kill
	Serial.begin(115200);

	// WiFi.mode(m): set mode to WIFI_AP, WIFI_STA, or WIFI_AP_STA.
	WiFi.mode(WIFI_STA);
	delay(10);

	pinMode(RollDownPin, OUTPUT);
	pinMode(secondLedPin, OUTPUT);
	pinMode(RollUpPin, OUTPUT);

	// Avoid being ON before someone connects
	digitalWrite(RollDownPin, LEDstate1);
	digitalWrite(secondLedPin, LEDstate2);
	digitalWrite(RollUpPin, LEDstate3);

	WiFi.begin(ssid, password);

	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
	}
	// Start the server
	server.begin();

	Serial.println("IP address: ");
	Serial.print(WiFi.localIP());
	// server.on("/", handleGenericArgs); //Associate the handler function to the path
}

void loop() {
	if (irrecv.decode(&results)) {
		Serial.println((long int)results.value, HEX);
		switch((long int)results.value) {
			case 0x20DFF10E : RollUp(32000); break;
			case 0x20DF0DF2 : RollUp(1000); break;
			case 0x20DF5DA2 : RollDown(1000); break;
			case 0x20DF718E : RollDown(28000); break;
			default : break;
		}
		irrecv.resume(); // Receive the next value
		return; // Kill the cycle
	}
	if(WiFi.status() != WL_CONNECTED) {
		Serial.println("Wireless NOT connected.");
		delay(500); // Do not use this delay in SoftAP mode
	}
//  else
//  {
//    Serial.println("WL connected.");
//  }
  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
//    Serial.println("No client connected, suiciding.");
    return;
  }

  int insanity = 0;
  // Wait until the client sends some data
  while (!client.available()) {
    insanity++;
    if (insanity == 1000)
    {
      Serial.println("And nobody came...");
      return;
    }
    delay(1);
    Serial.println("Waiting for client to send data.");
//    client = server.available(); // Check if the connection didn't fuck up, if yes, kill it.
//    if (!client) {
//    return;
//    }
  }
  Serial.println("Client available, receiving data...");

  // Read the first line of the request
  String request = client.readStringUntil('\r');
  client.flush();

  // Match the request
  if (request.indexOf("/DOWN") != -1)
    RollDown(28000);
  if (request.indexOf("/DOLU1") != -1)
    RollDown(1000);
  if (request.indexOf("/UP") != -1)
    RollUp(32000);
  if (request.indexOf("/NAHORU1") != -1)
    RollUp(1000);

  // Return the response
  String html = String("HTTP/1.1 200 OK\r\n") +
                "Content-Type: text/html\r\n" +
                "\r\n" +
                "<!DOCTYPE HTML>" +
                "<html>" +
                "<head>" +
                "<style media=\"screen\" type=\"text/css\">" +
                "   .button {" +
                "        background-color: #000000;" +
                "        color: #FFFFFF;" +
                "        padding: 10px;" +
                "        border-radius: 10px;" +
                "        -moz-border-radius: 10px;" +
                "        -webkit-border-radius: 10px;" +
                "        margin:10px" +
                "    }"
                "    .small-btn {" +
                "        width: 50px;" +
                "        height: 25px;" +
                "    }" +
                "    .medium-btn {" +
                "        width: 70px;" +
                "        height: 30px;" +
                "    }" +
                "    .big-btn {" +
                "        width: 90px;" +
                "        height: 40px;" +
                "    }" +
                "</style>" +
                "</head>" +
                "<body>" +
                "<a href=\"/DOWN\"><div class=\"button big-btn\">Down</div></a>" +
                "<a href=\"/DOLU1\"><div class=\"button medium-btn\">Down - 1s</div></a>" +
                "<a href=\"/UP\"><div class=\"button big-btn\">Up</div></a>" +
                "<a href=\"/NAHORU1\"><div class=\"button medium-btn\">Up - 1s</div></a>" +
                "</body>" +
                "</html>";
  client.print(html);
  delay(1);
}
